# scrna_demultiplexing

## What is it

This is a pipeline to demultiplex single cell data after alignment using Seurat. The pipeline is based on nextflow and uses the docker container for R execution environment. The pipeline can be used either with Semares as well as stand-alone.

The pipeline requires a hashtag file used by Seurat by alignment. The example hashtag file:
```
hash_blood,blood,R2,5PNNNNNNNNNN(BC),GTCAACTCTTTAGCG,Antibody Capture
hash_kidney,kidney,R2,5PNNNNNNNNNN(BC),TGATGGCCTATTGGG,Antibody Capture
```

## Standalone usage

For standalone usage, install `nextflow` and run the pipeline:

```
nextflow run https://gitlab.com/semares_pipelines/scrna_demultiplexing.git \
                  -profile docker \
                  --input_files <list_of_input_files> \
                  --output_folder <output_folder> \
                  --hashtags_file <hashtags_file> \
                  --demult_quantile <demultiplex_quantile> \
                  --cells_heatmap <cells_heatmap>
```

- `<input_files>` should contain exactly one .h5 file or three files (matrix.mtx, genes.tsv or features.tsv and barcodes.tsv) containing scRNA count matrix (from CellRanger, pipeline step1).
- `<output_folder>` is a folder for output 
- `<hashtags_file>` is a file used by CellRanger for alignment (s. example above)
- `<demult_quantile>` is a quantile used for demultiplexing (default 0.99)
- `<cells_heatmap>` is a maximum number of cells in the heatmap

The result contains:
SeuObj_ALL.rds  SeuObj_blood.rds  SeuObj_kidney.rds  SeuObj_onlySinglets.rds


- 	`SeuObj_ALL.rds` - Seurat object containing ALL cells
- 	`SeuObj_<tissue>.rds` - Seurat objects containing demultiplexed data for tissues
- 	`SeuObj_onlySinglets.rds` - Seurat object containing only singlets
-   various graphs in .png format

## Semares usage

To use the pipeline with Semares, You need the following steps:

- go to `persistance/workflow_adapter`
- clone the pipeline:
```
git clone https://gitlab.com/semares_pipelines/scrna_demultiplexing.git
```
- add a record to the `pipelines.yaml`:
```
- pipelineName: scrna_demultiplexing_pipeline
  pipelineDescription: pipeline for demultiplexing scRNA data after alignment
  pipelinePath: <semares_base>/persistence/workflow_adapter/scrna_demultiplexing/main.nf
  pipelineCommand: nextflow run -r main -profile docker
  pipelineParams:
  - paramName: Input dataset
    paramKey: --input_files
    paramDescription: input files
    paramType: InputPath
    isMultiValue: true
    isRequired: true
  - paramName: Output Folder Path
    paramDescription: output path
    paramKey: --output_folder
    paramType: OutputPath
    isMultiValue: false
    isRequired: true
  - paramName: Hashtags file
    paramKey: --hashtags_file
    paramDescription: hashtags file used by CellRanger for alignment
    paramType: InputPath
    isMultiValue: false
    isRequired: true
  - paramName: Demultiplexing quantile
    paramKey: --demult_quantile
    paramDescription: quantile used for demultiplexing
    paramType: Number
    isMultiValue: false
    isRequired: true
  - paramName: Number of Cells in Heatmap
    paramKey: --cells_heatmap
    paramDescription: maximum number of cells in the heatmap
    paramType: Number
    isMultiValue: false
    isRequired: true

```

## Future work

- single HTML/PDF report
