// general input and params
if (params.input_files){
    input_files = params.input_files.tokenize(",").collect{file(it, checkIfExists: true)}
}

ch_hashtags_file_exp = ((!params.hashtags_file_exp) || (params.hashtags_file_exp == "true"))?file("NONE"):file(params.hashtags_file_exp)
ch_hashtags_file_samp = ((!params.hashtags_file_samp) || (params.hashtags_file_samp == "true"))?file("NONE"):file(params.hashtags_file_samp)
ch_output_folder = file(params.output_folder)
ch_metadata_file = file(params.metadata_file)

workflow DEMULTIPLEX {
    DEMULTIPLEX_PROCESS (
         input_files,
         ch_hashtags_file_exp,
         ch_hashtags_file_samp,
         ch_metadata_file,
         params.demult_quantile,
         params.cells_heatmap,
         params.use_hashtag_ids
    )
}

process DEMULTIPLEX_PROCESS {

    container "${(params.containers_local) ? 'semares_demultiplex:1.0.0' : 
              'registry.gitlab.com/semares_pipelines/scrna_demultiplexing/semares_demultiplex:1.0.0' }"

    input:
        val input_files
        path hashtags_file_exp
        path hashtags_file_samp
        path metadata_file
        val demult_quantile
        val cells_heatmap
        val use_hashtag_ids

    output:
        path("*")
    
    script:
        """
        demultiplex.R \\
           --input_files ${input_files.join(",")} \\
           --output_folder "./" \\
           --hashtags_file_exp $hashtags_file_exp \\
           --hashtags_file_samp $hashtags_file_samp \\
           --demult_quantile $demult_quantile \\
           --cells_heatmap $cells_heatmap \\
           --metadata_file $metadata_file \\
           --use_hashtag_ids $use_hashtag_ids
        """  
}
